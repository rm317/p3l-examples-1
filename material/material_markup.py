# This applies a markup to material price by extracting it from the workpiece.
# You can set your material markup with a default value, and modify it at quote time.

markup = var('Markup Percentage', 0, 'Percent markup on material cost', number)
material_cost = get_workpiece_value('material_cost', 0)

PRICE = material_cost * markup / 100
DAYS = 0
