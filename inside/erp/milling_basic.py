# This milling operation is an extension of the work center operation template, but contextualizes it
# for milling processes.
#
# When working with runtime or setup_time in your pricing formula, time will always be in hours.  The display units
# can be set on the operation level or when quoting.

units_in()
mill = analyze_mill3()

setup_time = var('setup_time', 0, 'Setup time, specified in hours', number)
runtime = var('runtime', 0, 'Runtime, specified in hours', number)

setup_labor_rate = var('Setup Labor Rate', 0, 'Labor cost per hour to setup work center', currency)
run_labor_rate = var('Run Labor Rate', 0, 'Labor cost per hour to attend work center machine', currency)
crew = var('Crew', 1, 'Number of people assigned to attend work center', number)
machine_rate = var('Machine Rate', 0, 'Cost per hour to keep work center machine occupied', currency)
efficiency = var('Efficiency', 100, 'Percentage of real time the work center is running with regards to runtime', number)
percent_attended = var('Percent Attended', 0, 'Percentage of time work center must be attended', number)

total_cycle_time = part.qty * runtime
total_machine_occupied_time = total_cycle_time / efficiency * 100
total_attended_time = total_machine_occupied_time * percent_attended / 100

setup_cost = setup_time * setup_labor_rate
work_center_usage_cost = total_machine_occupied_time * machine_rate
run_labor_cost = total_attended_time * run_labor_rate * crew

# populate costs to workpiece so they can be referenced in later ops
set_workpiece_value('labor_cost', get_workpiece_value('labor_cost', 0) + setup_cost + run_labor_cost)
set_workpiece_value('machine_cost', get_workpiece_value('machine_cost', 0) + work_center_usage_cost)

PRICE = setup_cost + work_center_usage_cost + run_labor_cost
DAYS = 0