# This milling operation is an extension of the work center operation template, but contextualizes it
# for milling processes. It additionally uses the Paperless Parts generated runtime values and setup
# count to estimate price. We also establish a conservative material removal rate to act as a governor
# on very high runtime estimates. The same efficiency and percent attended logic applies to this operation.
# This formula will dynamically check whether the operation is deployed within a one-operation-per-setup
# (CNC) style process, or an all-setups-in-one-operation (Generic) style process and calculate prices accordingly.
#
# When working with runtime or setup_time in your pricing formula, time will always be in hours.  The display units
# can be set on the operation level or when quoting.

units_in()
mill = analyze_mill3()

setup_labor_rate = var('Setup Labor Rate', 0, 'Labor cost per hour to setup work center', currency)
run_labor_rate = var('Run Labor Rate', 0, 'Labor cost per hour to attend work center machine', currency)
crew = var('Crew', 1, 'Number of people assigned to attend work center', number)
machine_rate = var('Machine Rate', 0, 'Cost per hour to keep work center machine occupied', currency)
efficiency = var('Efficiency', 100, 'Percentage of real time the work center is running with regards to runtime', number)
percent_attended = var('Percent Attended', 0, 'Percentage of time work center must be attended', number)

# putting upper limit to runtime based on minimum material removal rate
removal_rate = var('Minimum Material Removal Rate', 1, 'Material removal rate in cu.in./min', number)
buffer = var('Stock Buffer, in', 0.25, 'Buffer to apply to part for when using material removal rate', number)
bbox = (part.size_x + buffer) * (part.size_y + buffer) * (part.size_z + buffer)
max_runtime = (bbox - part.volume) / removal_rate / 60

# this is used to detect whether the Per Setup flag is checked in the process template for CNC style process
is_per_setup = INDEX != None

setup_time_per_setup = var('Setup Time Per Setup', 0.5, 'Setup time per setup in hours', number)
setup_time = var('setup_time', 0, 'Setup time, specified in hours', number, frozen=False)
if is_per_setup:
    setup_time.update(setup_time_per_setup)
else:
    setup_time.update(mill.setup_count * setup_time_per_setup)
setup_time.freeze()

if is_per_setup:
    paperless_runtime_estimate = mill.setups[INDEX].runtime
else:
    paperless_runtime_estimate = mill.runtime

runtime = var('runtime', 0, 'Runtime per part, specified in hours', number, frozen=False)

has_been_interrogated = mill.runtime != 0
if has_been_interrogated:
    runtime.update(min(paperless_runtime_estimate, max_runtime))
else:
    runtime.update(max_runtime)
runtime.freeze()

# now apply efficiency and yield mathematics
total_cycle_time = part.qty * runtime
total_machine_occupied_time = total_cycle_time / efficiency * 100
total_attended_time = total_machine_occupied_time * percent_attended / 100

setup_cost = setup_time * setup_labor_rate
work_center_usage_cost = total_machine_occupied_time * machine_rate
run_labor_cost = total_attended_time * run_labor_rate * crew

# populate costs to workpiece so they can be referenced in later ops
set_workpiece_value('labor_cost', get_workpiece_value('labor_cost', 0) + setup_cost + run_labor_cost)
set_workpiece_value('machine_cost', get_workpiece_value('machine_cost', 0) + work_center_usage_cost)

PRICE = setup_cost + work_center_usage_cost + run_labor_cost
DAYS = 0